//
//  BaseTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

protocol GenericTableCellDeleteProtocol {
    func removeTableCell(_ indexPath: IndexPath)
}

class BaseTableViewCell: UITableViewCell {
    
    // MARK:- Outlets
    @IBOutlet var selectorIndicators: [UIView]!
    var delegate: GenericTableCellDeleteProtocol?
    
    //MARK:- Properties
    var mainData: Any?
    var indexPath: IndexPath!
    var isLastCell: Bool!
    var isFirstCell: Bool!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /// Apstract function for config cell
    ///
    /// - Parameters:
    ///   - data: Cell data - any object
    ///   - islastCell: Bool as indicator is cell last in section
    func configCell(_ data: Any?,isFirstCell:Bool, isLastCell: Bool, indexPath: IndexPath){
        self.mainData = data
        self.isFirstCell = isFirstCell
        self.isLastCell = isLastCell
        self.indexPath = indexPath
    }
    
    /// Apstact function from delete cell from cell itself
    func deleteCell(){
        delegate?.removeTableCell(indexPath)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        setIndicatorsSelected(animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        setIndicatorsSelected(highlighted)
    }
    
    // Configure the indicators for the selected/highlighted state
    func setIndicatorsSelected(_ selected: Bool){
        
        if selectorIndicators != nil{
            selectorIndicators!.forEach{
                if $0.isKind(of: UIImageView.self){
                    ($0 as! UIImageView).isHighlighted = selected
                }
                else {
                    $0.backgroundColor = $0.backgroundColor?.inverted
                }
            }
        }
    }

    
}

extension UIColor {
    var inverted: UIColor {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        UIColor.red.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: (1 - r), green: (1 - g), blue: (1 - b), alpha: a)
    }
}
