//
//  BaseHeaderFooterView.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BaseHeaderFooterView: UITableViewHeaderFooterView {


    //MARK:- Properties
    var mainData: Any?
    var section: Int?
    
    func configSectionView(_ data: Any?, section: Int){
        mainData = data
        self.section = section
    }

}
