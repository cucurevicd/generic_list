//
//  BaseCollectionViewCellWithTableView.swift
//  DemoApp
//
//  Created by Apple on 12/1/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BaseCollectionViewCellWithTableView: BaseCollectionViewCell {
    
    lazy var tableVC = { () -> GenericTableViewController in
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenericTableViewController")
        return vc as! GenericTableViewController
    }()

    /// Apstract function for config cell
    ///
    /// - Parameters:
    ///   - data: Cell data - any object
    ///   - islastCell: Bool as indicator is cell last in section
    override func configCell(_ data: Any?, isLastCell: Bool, indexPath: IndexPath) {
        super.configCell(data, isLastCell: isLastCell, indexPath: indexPath)
    }
    
    func addViewControllerToParentView(parentVC: UIViewController){
        
        parentVC.addChildViewController(tableVC)
        tableVC.didMove(toParentViewController: parentVC)
        self.contentView.addSubview(tableVC.view)
        tableVC.tableViewConfig((mainData as! PageContentSection).pageSections!, itemSelected: nil, itemDeselected: nil, itemDeleted: nil)
    }
    
    func removeControllerFromParentView(){
        tableVC.view.removeFromSuperview()
        tableVC.willMove(toParentViewController: nil)
        tableVC.removeFromParentViewController()
    }
}
