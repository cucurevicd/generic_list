//
//  BaseHeaderCollectionReusableView.swift
//  DemoApp
//
//  Created by Apple on 11/24/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BaseHeaderCollectionReusableView: UICollectionReusableView {
    
    //MARK:- Properties
    var mainData: Any?
    var section: Int?
    
    func configSectionView(_ data: Any?, section: Int){
        mainData = data
        self.section = section
    }
    
}
