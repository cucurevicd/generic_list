//
//  BaseCollectionViewCell.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

protocol GenericCollectionCellDeleteProtocol {
    func removeCollectionCell(_ indexPath : IndexPath)
}

class BaseCollectionViewCell: UICollectionViewCell {
    
    // MARK:- Outlets
    @IBOutlet var selectorIndicators: [UIView]!
    var delegate: GenericCollectionCellDeleteProtocol?
    
    //MARK:- Properties
    var mainData: Any?
    var indexPath: IndexPath!
    var isLastCell: Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    /// Apstract function for config cell
    ///
    /// - Parameters:
    ///   - data: Cell data - any object
    ///   - islastCell: Bool as indicator is cell last in section
    func configCell(_ data: Any?, isLastCell: Bool, indexPath: IndexPath){
        self.mainData = data
        self.isLastCell = isLastCell
        self.indexPath = indexPath
    }
    
    /// Apstact function from delete cell from cell itself
    func deleteCell(){
        delegate?.removeCollectionCell(indexPath)
    }
    
    override var isSelected: Bool{
        didSet{
            setIndicatorsSelected(isSelected)
        }
    }
    
    override var isHighlighted: Bool{
        didSet{
            setIndicatorsSelected(isHighlighted)
        }
    }
    
    // Configure the indicators for the selected/highlighted state
    func setIndicatorsSelected(_ selected: Bool){
        
        if selectorIndicators != nil{
            selectorIndicators!.forEach{
                if $0.isKind(of: UIImageView.self){
                    ($0 as! UIImageView).isHighlighted = selected
                }
                else {
                    $0.backgroundColor = $0.backgroundColor?.inverted
                }
            }
        }
    }
}
