//
//  GenericCollectionViewController.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

protocol GenericCollectionOffsetIndicator {
    func pageOffset(_ yOffest: CGFloat)
}

protocol GenericCollectionSelectedCellIndicator {
    func pageCurrent(_ currentPage: Int, collectionView: GenericCollectionViewController)
}

@IBDesignable
class GenericCollectionViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var collectionView: UICollectionView!
    @IBInspectable var pullToRefresh: Bool = false
    @IBInspectable var cellSize: CGSize = CGSize.zero // Zero create automatic cell size
    
    var pageOffsetIndicatorDelegate: GenericCollectionOffsetIndicator?
    var pageCurrentIndicatorDelegate: GenericCollectionSelectedCellIndicator?
    var refreshContorller: UIRefreshControl?
    
    var dataSource: [SectionData]?{
        didSet{
            if dataSource != nil{
                registerCells()
                self.setCellHeight()
                collectionView.reloadData()
                createSelectedArray()
            }
        }
    }
    var selectedSectionsData = [SectionData]()
    private var selectHandler : ItemSelectedHandler?
    private var deselectHandler: ItemDeselected?
    private var deleteHandler: ItemDeletedHandler?
    
    // MARK:- Config table view
    func collectionViewConfig(_ dataSource:[SectionData], itemSelected: ItemSelectedHandler?, itemDeselected: ItemDeselected?, itemDeleted:ItemDeletedHandler?){
        
        self.dataSource = dataSource
        selectHandler = itemSelected
        deselectHandler = itemDeselected
        deleteHandler = itemDeleted
    }
    
    // MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        setPullToRefresh()
    }
    
    // Add pull to refresh indicator
    private func setPullToRefresh(){
        
        if pullToRefresh{
            refreshContorller = UIRefreshControl()
            self.collectionView.addSubview(refreshContorller!)
            refreshContorller?.addTarget(self, action: #selector(GenericCollectionViewController.startReloadDataOnPull(_:)), for: .valueChanged)
        }
    }
    
    // Collection view height
    private func setCellHeight(){
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            if cellSize == CGSize.zero{
                flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
            }
            else{
                flowLayout.itemSize = cellSize
            }
        }
    }
    
    @objc private func startReloadDataOnPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        if let vc = parent as? BaseViewController{
            vc.loadingData()
        }
    }

    private func createSelectedArray(){
        
        while dataSource!.count > selectedSectionsData.count {
            selectedSectionsData.append(SectionData())
        }
    }
    
    // MARK:- Regsister cell
    
    /**
     If using xib for presenting cell, register it here for collection view
     #Important: Use for nibName and cellReuseIdentifier same string!!
     */
    private func registerCells() {
        dataSource!.forEach { (section) in
            
            // Section Header cell
            if let headerCell = section.headerReuseIdentifier{
                collectionView.register(UINib(nibName: headerCell, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerCell)
            }
            if let footerCell = section.footerReuseIdentifier{
                collectionView.register(UINib(nibName: footerCell, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerCell)
            }
            // Section cell for page content
            if let pageContent = section as? PageContentSection{
                // Cell is set in storyboard
                return
            }
            // Regular cell
            else{
                section.rowDatas.forEach({ (row) in
                    if let cellId = row.cellIdentifier{
                        registerCellForIdentifier(cellId)
                    }
                })
            }
        }
        // Loading cell
        registerCellForIdentifier("LoadingTableCell")
    }
    
    /**
     Register cell for table view
     
     - parameter cellIdentifier: The reuse identifier
     */
    private func registerCellForIdentifier(_ cellIdentifier: String) {
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK:- Collection view
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if dataSource is [PageContentSection]{
            return 1
        }
        
        return dataSource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let section = dataSource![section]
        
        if section is PageContentSection{
           
            return dataSource!.count
        }
        
        // If there is pagination
        if let pagination = section.pagination{
            if pagination{
                return section.rowDatas.count + 1
            }
        }
        
        return section.rowDatas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cellReuseId = String()
        let cellData : Any?
        let section = dataSource![indexPath.section]
        
        if let pageContentSection = section as? PageContentSection{
            cellReuseId = pageContentSection.pageCellId
            cellData = dataSource![indexPath.item] 
        }
        else{
            cellData = section.rowDatas[indexPath.row]
            
            // Pagination
            if indexPath.row == section.rowDatas.count{
                cellReuseId = "LoadingTableCell"
            }
                // Regular cell
            else {
                assert((cellData as? RowData)?.cellIdentifier != nil, "Insert cell identifier")
                cellReuseId = ((cellData as? RowData)?.cellIdentifier)!
            }
        }
        
        let lastCell = section.rowDatas.count - 1 == indexPath.row
        
        // Config cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseId, for: indexPath) as! BaseCollectionViewCell
        cell.configCell(cellData, isLastCell: lastCell, indexPath: indexPath)
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let section = dataSource![indexPath.section]
        var viewId = String()
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            if let headerId = section.headerReuseIdentifier{
                viewId = headerId
            }
            else{
                assert(false, "Header reuse id not set!")
            }
            break
        case UICollectionElementKindSectionFooter:
            if let footerId = section.footerReuseIdentifier{
                viewId = footerId
            }
            else{
                assert(false, "Footer reuse id not set!")
            }
            break
        default:
            assert(false, "Unexpected element kind")
        }
        
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: viewId, for: indexPath)
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = dataSource![indexPath.section]
        let cell : RowData = section.rowDatas[indexPath.row]
        
        if self.selectHandler != nil{
            self.selectHandler!(cell.cellData)
        }
        
        // Add selection to selected array
        let selectedSection = selectedSectionsData[indexPath.section]
        if !selectedSection.rowDatas.contains(cell){
            selectedSection.rowDatas.append(cell)
        }
        
        // For tracking selection of page in Page Controller
        if pageCurrentIndicatorDelegate != nil{
            self.pageCurrentIndicatorDelegate?.pageCurrent(indexPath.item, collectionView:self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        // This means that it is content collection view inside page controller view
        if cell is BaseCollectionViewCellWithTableView{
            (cell as! BaseCollectionViewCellWithTableView).addViewControllerToParentView(parentVC: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if cell is BaseCollectionViewCellWithTableView{
            (cell as! BaseCollectionViewCellWithTableView).removeControllerFromParentView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // Remove data from selection to selected array
        let selectedSection = selectedSectionsData[indexPath.section]
        let section = dataSource![indexPath.section]
        let cellData = section.rowDatas[indexPath.row]
        selectedSection.rowDatas = selectedSection.rowDatas.filter{ $0 == cellData}
        
        // Remove selection from selected array
        if self.deselectHandler != nil{
            self.deselectHandler!(cellData.cellData)
        }
    }
    
    // MARK:- Page indicator
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if pageOffsetIndicatorDelegate != nil{
         
            let page = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
            self.pageCurrentIndicatorDelegate?.pageCurrent(page, collectionView:self)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = scrollView.contentOffset.y
        self.pageOffsetIndicatorDelegate?.pageOffset(yOffset)
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pageControllerCellSegue"{
            
        }
    }
    
    // MARK:- Remove cell
    func removeItem(indexPath: IndexPath){
        let section = dataSource![indexPath.section]
        section.rowDatas.remove(at: indexPath.item)
        collectionView.deleteItems(at: [indexPath])
    }
    
}

extension GenericCollectionViewController: GenericCollectionCellDeleteProtocol{
    
    internal func removeCollectionCell(_ indexPath: IndexPath) {
        removeItem(indexPath: indexPath)
    }
}
