//
//  GenericTableViewController.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

typealias ItemSelectedHandler = (_ data: Any?) -> Void
typealias ItemDeselected = (_ data: Any?) -> Void
typealias ItemDeletedHandler = (_ data: Any?) -> Void

protocol EditingTableCellProtocol {
    
    func editinCellActions(tableVC: GenericTableViewController, indexPath:IndexPath) -> [UITableViewRowAction]
}

struct ExpandedAccordionData{
    
    var indexPath: IndexPath?
    var cells: AccordionData?
}

@IBDesignable
class GenericTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK:- Outlet
    @IBOutlet weak var tableView: UITableView!
    
    @IBInspectable var pullToRefresh: Bool = false
    var refreshContorller: UIRefreshControl?
    
    // MARK:- Params
    private var dataSource: [SectionData]?{
        didSet{
            if dataSource != nil{
                registerCells()
                tableView.reloadData()
                createSelectedArray()
            }
        }
    }
    
    var editigTableCellDelegate: EditingTableCellProtocol?
    var selectedSectionsData = [SectionData]()
    private var selectHandler : ItemSelectedHandler?
    private var deselectHandler: ItemDeselected?
    private var deleteHandler: ItemDeletedHandler?
    
    // Accordion
    var expandedAccordionData = [ExpandedAccordionData]()
    
    
    // MARK:- Config table view
    func tableViewConfig(_ dataSource:[SectionData], itemSelected: ItemSelectedHandler?, itemDeselected: ItemDeselected?, itemDeleted:ItemDeletedHandler?){
        
        self.dataSource = dataSource
        selectHandler = itemSelected
        deselectHandler = itemDeselected
        deleteHandler = itemDeleted
    }
    
    // MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setCellHeight()
        setPullToRefresh()
    }
    
    private func createSelectedArray(){
        
        while dataSource!.count > selectedSectionsData.count {
            selectedSectionsData.append(SectionData())
        }
    }
    
    // Add pull to refresh indicator
    func setPullToRefresh(){
        
        if pullToRefresh{
            refreshContorller = UIRefreshControl()
            self.tableView.addSubview(refreshContorller!)
            refreshContorller?.addTarget(self, action: #selector(GenericTableViewController.startReloadDataOnPull(_:)), for: .valueChanged)
        }
    }
    
    // Table view height
    private func setCellHeight(){

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableView.sectionFooterHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 140.0
        self.tableView.estimatedSectionHeaderHeight = 60.0
        self.tableView.estimatedSectionFooterHeight = 60.0
    }
    
    @objc private func startReloadDataOnPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        if let vc = parent as? BaseViewController{
            vc.loadingData()
        }
    }
    
    // MARK:- Remove cell from table view
    
    /// Remove cell from table view
    ///
    /// - parameter data:      data to be removed
    /// - parameter indexPath: Remove at index path
    private func deleteCell(indexPath: IndexPath) {
        
        let section = dataSource![indexPath.section]
        let data = section.rowDatas[indexPath.row]
        section.rowDatas.remove(at: indexPath.row)
        
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        }
        
        if deleteHandler != nil{
            deleteHandler!(data)
        }
    }
    
    private func insertCell(indexPath: IndexPath, rowData: RowData){
        
        let section = dataSource![indexPath.section]
        section.rowDatas.insert(rowData, at: indexPath.row)
        
        DispatchQueue.main.async {
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    // MARK:- Regsister cell
    
    /**
     If using xib for presenting cell, register it here for table view
     #Important: Use for nibName and cellReuseIdentifier same string!!
     */
    private func registerCells() {
        dataSource!.forEach { (section) in
         
            // Section Header cell
            if let headerCell = section.headerReuseIdentifier{
                tableView.register(UINib(nibName: headerCell, bundle: nil), forHeaderFooterViewReuseIdentifier: headerCell)
            }
            // Section Footer cell
            if let footerCell = section.footerReuseIdentifier{
                tableView.register(UINib(nibName: footerCell, bundle: nil), forHeaderFooterViewReuseIdentifier: footerCell)
            }
            // Section cell
            section.rowDatas.forEach({ (row) in
                if let cellId = row.cellIdentifier{
                    registerCellForIdentifier(cellId)
                }
            })
        }
        // Loading cell
        registerCellForIdentifier("LoadingTableCell")
    }
    
    /**
     Register cell for table view
     
     - parameter cellIdentifier: The reuse identifier
     */
    private func registerCellForIdentifier(_ cellIdentifier: String) {
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    //MARK:- Table view
    
    // MARK:- Data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionData = dataSource![section]
        
        // If there is pagination
        if let pagination = sectionData.pagination{
            if pagination{
                return sectionData.rowDatas.count + 1
            }
        }
        
//        // If accoordion expanded
//        var expandedCells = 0
//        expandedAccordionData.forEach{
//            if ($0.indexPath?.section)! == section{
//                expandedCells += ($0.cells?.childs?.count)!
//            }
//        }
        return sectionData.rowDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellReuseId = String()
        let section = dataSource![indexPath.section]
        let cellData : RowData = section.rowDatas[indexPath.row]
        let lastCell = section.rowDatas.count - 1 == indexPath.row
        let firstCell = indexPath.row == 0
        // Pagination
        if indexPath.row == section.rowDatas.count{
            cellReuseId = "LoadingTableCell"
        }
        else{
            assert(cellData.cellIdentifier != nil, "\nRowData must have cell Identifier!\n")
            cellReuseId = cellData.cellIdentifier!
        }
        
        // Config cell
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath) as! BaseTableViewCell
        cell.configCell(cellData.cellData, isFirstCell: firstCell, isLastCell: lastCell, indexPath: indexPath)
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }

    // MARK:- Height
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = dataSource![indexPath.section]
        let cellData : RowData = section.rowDatas[indexPath.row]
        return cellData.cellHeight != nil ? cellData.cellHeight! : UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        let section = dataSource![section]
        return section.footerHeight != nil ? section.footerHeight! : UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let section = dataSource![section]
        return section.headerHeight != nil ? section.headerHeight! : UITableViewAutomaticDimension
    }
    
    // MARK:- Sections View
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionData = dataSource![section]
        guard sectionData.headerReuseIdentifier == nil else {
            // Configure cell name
            let cellHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionData.headerReuseIdentifier!) as? BaseHeaderFooterView
            cellHeader?.configSectionView(sectionData.headerFooterData, section: section)
            return cellHeader
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sectionData = dataSource![section]
        guard sectionData.footerReuseIdentifier == nil else {
            // Configure cell name
            let cellHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionData.footerReuseIdentifier!) as? BaseHeaderFooterView
            cellHeader?.configSectionView(sectionData.headerFooterData, section: section)
            return cellHeader
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let section = dataSource![section]
        return section.title
    }
    
    // MARK:- Selection
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section = dataSource![indexPath.section]
        let cell : RowData = section.rowDatas[indexPath.row]
        
        if self.selectHandler != nil{
            self.selectHandler!(cell.cellData)
        }
        
        // Add selection to selected array
        let selectedSection = selectedSectionsData[indexPath.section]
        if !selectedSection.rowDatas.contains(cell){
            selectedSection.rowDatas.append(cell)
        }
        
        // Handle accordion
        if cell.accordionCells != nil{
            animateAccordion(cellData: cell, indexPath: indexPath, expand: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        // Remove data from selection to selected array
        let selectedSection = selectedSectionsData[indexPath.section]
        let section = dataSource![indexPath.section]
        let cellData = section.rowDatas[indexPath.row]
        selectedSection.rowDatas = selectedSection.rowDatas.filter{ $0 == cellData}
        
        // Remove selection from selected array
        if self.deselectHandler != nil{
            self.deselectHandler!(cellData.cellData)
        }
        
        // Handle accordion
        if cellData.accordionCells != nil{
            animateAccordion(cellData: cellData, indexPath: indexPath, expand: false)
        }
    }
    
    // MARK:- Editing cell
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return editigTableCellDelegate != nil
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        return self.editigTableCellDelegate?.editinCellActions(tableVC: self, indexPath: indexPath)
    }
    
}

//MARK:- Accordion
extension GenericTableViewController{
    
    func animateAccordion(cellData: RowData, indexPath: IndexPath, expand: Bool){
        
        let accordionData = ExpandedAccordionData(indexPath: indexPath, cells: cellData.accordionCells)
        var indexPaths = [IndexPath]()
        for idx in 1...(cellData.accordionCells?.childs?.count ?? 0) {
            
            let newIndex = IndexPath(row: indexPath.row + idx, section: indexPath.section)
            indexPaths.append(newIndex)
        }
        
        if expand{
            expandedAccordionData.append(accordionData)
            dataSource![indexPath.section].rowDatas.insert(contentsOf: cellData.accordionCells!.childs!, at: indexPath.row + 1)
        }
        else{
            expandedAccordionData = expandedAccordionData.filter{ $0.indexPath != indexPath}
            dataSource![indexPath.section].rowDatas.removeSubrange(indexPath.row + 1...(cellData.accordionCells?.childs?.count)!)
        }
        
        expand ? expandCell(indexPaths: indexPaths) : collapseCell(indexPaths: indexPaths)
    }
    
    func expandCell(indexPaths : [IndexPath]){
        
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: indexPaths, with: .top)
        self.tableView.endUpdates()
    }
    
    func collapseCell(indexPaths: [IndexPath]){
        
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: indexPaths, with: .top)
        self.tableView.endUpdates()
    }
}

// MARK:- Remove cell
extension GenericTableViewController: GenericTableCellDeleteProtocol{
   
    internal func removeTableCell(_ indexPath: IndexPath) {
        deleteCell(indexPath: indexPath)
    }
}
