//
//  BaseViewController.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK:- Properties
    var tableVC: GenericTableViewController?
    var collectionVC: GenericCollectionViewController?
    var pageVC: GenericPageViewController?

    // MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Check is view conotrller conform to proptocol
        if (self as? EditingTableCellProtocol) != nil{
            tableVC?.editigTableCellDelegate = self as? EditingTableCellProtocol
        }
    }


    //MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: GenericTableViewController.self){
            self.tableVC = segue.destination as? GenericTableViewController
        }
        else if segue.destination.isKind(of: GenericCollectionViewController.self){
            self.collectionVC = segue.destination as? GenericCollectionViewController
        }
        else if segue.destination.isKind(of: GenericPageViewController.self){
            self.pageVC = segue.destination as? GenericPageViewController
        }
    }
    
    
    /// Apstract function for loading data. Used to call when list come to pagination action
    func loadingData(){
    }

}


extension UIApplication {
    
    /**
     Get top most view controller
     - parameter base: Root view controller
     - returns: Top view controller
     */
    class func topViewController(_ base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController! {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
    
}
