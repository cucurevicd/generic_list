//
//  GenericPageViewController.swift
//  DemoApp
//
//  Created by Apple on 11/27/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit


typealias PageControllerCurrentPageHandler = (_ currentPage: Int) -> Void
typealias PageControllerYOffsetPageHandler = (_ yOffset: CGFloat) -> Void

@IBDesignable
class GenericPageViewController: UIViewController, GenericCollectionOffsetIndicator , GenericCollectionSelectedCellIndicator{
    
    //MARK:- Properties
    @IBInspectable var headerCellReuseIdentifier: String?
    @IBInspectable var headerEqualCellSize : Bool = false
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var headerContainer: UIView!
    
    // MARK:- Params
    var firstTimeLoad: Bool = true
    var controllerContentOffset: CGFloat?
    private var dataSource: PageData?
    
    private var currentPage : PageControllerCurrentPageHandler?
    private var yOffset: PageControllerYOffsetPageHandler?
    
    var headerCollectionView: GenericCollectionViewController?
    var pageControllerCollectionView: GenericCollectionViewController?


    // MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.configHeader()
        self.configContent()
        
        assert(dataSource!.pageHeaderData != nil, "Set header row data!")
        let headerData = SectionData(sectionData: dataSource!.pageHeaderData!)

        registerCells()
        
        // Config both collection views
        if headerEqualCellSize{
            let cellWidth = (headerCollectionView?.view.bounds.width)! / CGFloat(headerData.rowDatas.count)
            headerCollectionView?.cellSize = CGSize(width: cellWidth, height: (headerCollectionView?.view.bounds.height)!)
        }
        
        headerCollectionView!.collectionViewConfig([headerData], itemSelected: nil, itemDeselected: nil, itemDeleted: nil)
        pageControllerCollectionView!.collectionViewConfig(dataSource!.pageContentData, itemSelected: nil, itemDeselected: nil, itemDeleted: nil)
        
        // Set first tab activated
        if firstTimeLoad{
            self.pageCurrent(0, collectionView: pageControllerCollectionView!)
            firstTimeLoad = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "header"{
            self.headerCollectionView = segue.destination as? GenericCollectionViewController
        }
        else if segue.identifier == "content"{
            self.pageControllerCollectionView = segue.destination as? GenericCollectionViewController
        }
    }
    
    
    //MARK:- Config collection properties
    
    func configHeader(){
        
        headerCollectionView!.cellSize = CGSize(width: 100, height: headerContainer.bounds.height)
        headerCollectionView!.pageCurrentIndicatorDelegate = self
        
        if let layout = headerCollectionView!.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.sectionInset = UIEdgeInsets.zero
            layout.minimumLineSpacing = 0.0
        }
    }
    
    func configContent(){
        self.view.setNeedsLayout()
        if let layout = pageControllerCollectionView!.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.sectionInset = UIEdgeInsets.zero
            layout.minimumLineSpacing = 0.0
        }
        pageControllerCollectionView!.collectionView.allowsSelection = false
        pageControllerCollectionView!.cellSize = (pageControllerCollectionView?.view.bounds.size)!
        pageControllerCollectionView!.collectionView.isPagingEnabled = true
        pageControllerCollectionView!.pageOffsetIndicatorDelegate = self
        pageControllerCollectionView!.pageCurrentIndicatorDelegate = self
    }
    
    
    /// Config page contorller with data for header collection view and page content
    ///
    /// - Parameters:
    ///   - pageData: Data for every page with title (presented in header cell for current page)
    ///   - currentPage: Callback when page is visiable
    ///   - offset: Offset of collection view in Y-axis
    func configPageController(pageData : PageData, currentPage: PageControllerCurrentPageHandler?, offset: PageControllerYOffsetPageHandler?){
        
        self.dataSource = pageData
        self.currentPage = currentPage
        self.yOffset = offset
    }
    
    // MARK:- Offset indicator of page content
    func pageOffset(_ yOffest: CGFloat) {
        
        self.controllerContentOffset = yOffest
    }
    
    func pageCurrent(_ currentPage: Int, collectionView: GenericCollectionViewController) {
        
        let index = IndexPath(item: currentPage, section: 0)
        
        if collectionView == headerCollectionView{
                self.pageControllerCollectionView!.collectionView.scrollToItem(at: index, at: .right, animated: true)
        }
        else if collectionView == pageControllerCollectionView{
                self.headerCollectionView!.collectionView.selectItem(at: index, animated: true, scrollPosition: .left)
                self.headerCollectionView!.collectionView(collectionView.collectionView, didSelectItemAt: index)
        }
    }
    
    
    private func registerCells(){
        
        self.pageControllerCollectionView!.collectionView.register(BaseCollectionViewCellWithTableView.self, forCellWithReuseIdentifier: "BaseCollectionViewCellWithTableView")
        assert(headerCellReuseIdentifier != nil, "Enter header cell reuse identifier!")
        self.headerCollectionView!.collectionView.register(UINib(nibName: headerCellReuseIdentifier!, bundle: nil), forCellWithReuseIdentifier: headerCellReuseIdentifier!)
    }
}
