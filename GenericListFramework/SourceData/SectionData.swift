//
//  SectionData.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

class SectionData: NSObject{
    
    // MARK:- Params
    var title:String?
    var headerReuseIdentifier: String?
    var footerReuseIdentifier: String?
    var headerFooterData: Any?
    var rowDatas = [RowData]()
    var pagination: Bool?
    
    var headerHeight: CGFloat?
    var footerHeight: CGFloat?
    
    //MARK:- Methods
    
    override init() {
        super.init()
    }
    /// Init section data with header title and rowDatas
    ///
    /// - Parameters:
    ///   - headerTitle: Title of header. This property will be set in "func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {}"
    ///   - rowDatas: Cells data
    init(_ headerTitle:String?, rowDatas : [RowData]) {
        super.init()
        
        self.title = headerTitle
        self.rowDatas = rowDatas
    }
    
    /// Init with custom reusable header footer View
    ///
    /// - Parameters:
    ///   - headerReuseId: Reuse id of header view. Important!! View Class must be same name as Xib name
    ///   - headerData: Data to conifg custom view header
    ///   - rowDatas: Cells data
    init(_ headerReuseId: String?, headerData: Any?, rowDatas: [RowData]) {
        super.init()
        
        self.headerReuseIdentifier = headerReuseId
        self.headerFooterData = headerData
        self.rowDatas = rowDatas
    }
    
    
    /// Use this init when you need data for header in page controller
    ///
    /// - Parameter sectionData: Page controller header data
    init(sectionData: [RowData]) {
        super.init()
        
        self.rowDatas = sectionData
    }
}
