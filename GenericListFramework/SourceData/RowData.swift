//
//  RowData.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import UIKit

class RowData: NSObject{
    
    var cellIdentifier: String?
    var cellData: Any?
    var accordionCells : AccordionData?
    
    var cellHeight: CGFloat?
    
    init(cellId: String, cellData:Any?, accordionData: AccordionData? = nil) {
        super.init()
        self.cellIdentifier = cellId
        self.cellData = cellData
        self.accordionCells = accordionData
    }
}

struct AccordionData {
    var childs: [RowData]?
}
