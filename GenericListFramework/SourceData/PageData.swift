//
//  PageData.swift
//  DemoApp
//
//  Created by Apple on 11/27/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PageContentSection : SectionData{
    
    let pageCellId = "BaseCollectionViewCellWithTableView"
    var pageSections : [SectionData]?
    
    init(pageSections: [SectionData]) {
        super.init()
        self.pageSections = pageSections
    }
}

class PageData: NSObject {

    var pageHeaderData: [RowData]?
    var pageContentData = [PageContentSection]()
    
    init(headerData: [RowData],contentData:[PageContentSection] ) {
        super.init()
        
        self.pageHeaderData = headerData
        self.pageContentData = contentData
    }
    
    /// Init with tile for header of page, content of table view inside collection view cell, and index of page in collection view
    ///
    /// - Parameters:
    ///   - title: Title of page
    ///   - content: Content of page
    ///   - index: Index of page
    init(headerData: [RowData]?, content: [[SectionData]]) {
        super.init()
        self.pageHeaderData = headerData
        
        content.forEach{
            let pageContent = PageContentSection(pageSections: $0)
            self.pageContentData.append(pageContent)
        }
    }
}
