//
//  HomeCollectionViewController.swift
//  DemoApp
//
//  Created by Apple on 11/24/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class HomeCollectionViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let cell = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
        let cell2 = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
        let cell3 = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
        let section = SectionData("First section", rowDatas: [cell, cell2, cell3])
        
        let cell12 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
        let cell22 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
        let cell23 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
        let section2 = SectionData("Second section", rowDatas: [cell12, cell22, cell23])
        
        self.collectionVC?.collectionViewConfig([section, section2],
                                                itemSelected: { (data) in
                                                    print(data as Any)
        },
                                                itemDeselected: { (data) in
                                                    print(data as Any)
        },
                                                itemDeleted: { (data) in
                                                    print(data as Any)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
