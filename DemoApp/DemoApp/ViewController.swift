//
//  ViewController.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    var person: Person?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        person = Person()
        person?.name = "Test"
        person?.lastName = "Lastname"
        
        person?.addObserver(self, forKeyPath: "name", options: .new, context: nil)
        
        let cell = RowData(cellId: "FirstTableViewCell", cellData: person)
        let cell2 = RowData(cellId: "FirstTableViewCell", cellData: "Bye")
        let cell3 = RowData(cellId: "FirstTableViewCell", cellData: "Qwerw")
        let section = SectionData("First section", rowDatas: [cell, cell2, cell3])
        
        
        let accordions = AccordionData(childs: [cell, cell2, cell3])
        let cell12 = RowData(cellId: "SecondTableViewCell", cellData: nil,accordionData: accordions)
        let section2 = SectionData("Second section", rowDatas: [cell12])
        
        self.tableVC?.tableView.allowsMultipleSelection = true
        self.tableVC?.tableViewConfig([section, section2],
                                      itemSelected: { (selectedItem) in
         
                                        print(self.person as Any)
                                        print("Selected item: \(selectedItem ?? "")")
        },
                                      itemDeselected: { (deselectItem) in
                                        
                                        print("Deselect item: \(deselectItem ?? "")")
        },
                                      itemDeleted: { (deletedItem) in
                                        
                                        print("Deleted item : \(deletedItem ?? "")")
        })
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        print(object as Any)
    }

}

extension UIViewController : EditingTableCellProtocol{
    
    func editinCellActions(tableVC: GenericTableViewController, indexPath: IndexPath) -> [UITableViewRowAction] {
     
        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = .lightGray
        
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = .orange
        
        let share = UITableViewRowAction(style: .destructive, title: "Share") { action, index in
            print("share button tapped")
//            tableVC.removeTableCell(indexPath)
        }
        share.backgroundColor = .blue
        
        return [share, favorite, more]
    }
}

