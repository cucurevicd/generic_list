//
//  PageContentViewController.swift
//  DemoApp
//
//  Created by Apple on 11/27/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PageContentViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        let header = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "First tab")
        let header2 = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "Second tab")
        let header3 = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "Second tab")
        
        let cell = RowData(cellId: "FirstTableViewCell", cellData: nil)
        let cell22 = RowData(cellId: "SecondTableViewCell", cellData: nil)
        let cell2 = RowData(cellId: "FirstTableViewCell", cellData: nil)
        let cell3 = RowData(cellId: "SecondTableViewCell", cellData: nil)
        let cell4 = RowData(cellId: "FirstTableViewCell", cellData: nil)
        
        let section = SectionData("First section", rowDatas: [cell, cell22, cell2, cell3, cell4])
        let section2 = SectionData("Second section", rowDatas: [cell, cell22, cell2])
        let section3 = SectionData("Third section", rowDatas: [cell, cell22, cell2, cell4])
        
        let page1 = PageContentSection(pageSections: [section, section2])
        let page2 = PageContentSection(pageSections: [section2])
        let page3 = PageContentSection(pageSections: [section3])
        
        let pageControllerData = PageData(headerData: [header,header2, header3], contentData: [page1, page2, page3])
        
        self.pageVC?.configPageController(pageData: pageControllerData, currentPage: { (currentPage) in
            print("View controller current page: \(currentPage)")
        }, offset: { (offset) in
            print("View controller current offset: \(offset)")
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
