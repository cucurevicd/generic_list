//
//  SecondCollectionViewCell.swift
//  DemoApp
//
//  Created by Apple on 11/24/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class SecondCollectionViewCell: BaseCollectionViewCell {
    
    override func configCell(_ data: Any?, isLastCell: Bool, indexPath: IndexPath) {
        super.configCell(data, isLastCell: isLastCell, indexPath: indexPath)
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        deleteCell()
    }
}
