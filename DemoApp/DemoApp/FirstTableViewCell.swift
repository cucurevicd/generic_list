//
//  FirstTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 11/23/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class Person: NSObject {
    @objc dynamic var name: String?
    var lastName: String?
}

class FirstTableViewCell: BaseTableViewCell , UITextFieldDelegate{

    
    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func configCell(_ data: Any?,isFirstCell: Bool, isLastCell: Bool, indexPath: IndexPath) {
        super.configCell(data,isFirstCell: isFirstCell, isLastCell: isLastCell, indexPath: indexPath)
        
        self.textField.delegate = self
        if let name = (data as? Person)?.name{
        
            self.textField.text = name
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        (mainData as! Person).name = textField.text
    }
    
}
