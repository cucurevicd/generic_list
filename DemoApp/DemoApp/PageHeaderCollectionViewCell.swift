//
//  PageHeaderCollectionViewCell.swift
//  DemoApp
//
//  Created by Apple on 11/27/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class PageHeaderCollectionViewCell: BaseCollectionViewCell {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    override var isSelected: Bool{
        didSet{
            self.backgroundColor = isSelected ? UIColor.brown : UIColor.white
        }
    }
    
    override func configCell(_ data: Any?, isLastCell: Bool, indexPath: IndexPath) {
        
        if let title = data as? String{
            self.headerTitleLabel.text = title
        }
    }
}
