# README #

This readme document describe purpose of GenericList framework, how to setup and how to use it.

### What is this repository for? ###


* Quick summary
Creating of collection and table view use lot of boilerplate code. Implementing collection and table view (lists) is composit of setting data source and delegate methods. Using this method needs to be rewrite for every list, and what is generaly changed is data, which is data source of list and graphical presentation of that data. In many apps, we need to create list with several cells which are reused in all design. Because of that, idea is to create generic list, which will reuse graphical content and data for cells, and to avoid creating biolerplate code.

Main idea is that every cell have it data, which is used to populate it graphical content. Observing list generaly, to create list, we need to know what data is used by cells, and what kind of graphical representation of cell is used.
Because of that our object which will be used for creating cells is **RowData**.

This idea is implemented in both collection and table view.
In regard to implementing of lists in ViewController, lists are child view contorllers, which are graphicly implemented as embeded controllers in container.

* Version
**1.0.0**

### How do I get set up? ###

* Main setup
* Storyboard setup
* Source data types
* GenericTableViewContorller setup
* GenericCollectionViewController setup
* GenericPageViewController setup

##### Main setup #####

Copy GenericListFramework folder to your project, and from Main.storyboard copy GenericTableViewController, GenericCollectionViewController and GenericPageViewController to your storyboard.

##### Storyboard setup #####

Storyboard files are generated in Main.storyboard. To overcome setting of GenericListViewControllers in every view contorller where we want to implement list, I have created storyboard files. You can use GenericTableViewController, GenericCollectionViewController, GenericPageController,based on your need.
To create list in your viewController, in xib of view controller create container in view controller view, and create segue to GenericTableViewController. For type of segue link choose **embeded**.

Implementing in view controller is automatic, based on certain circumstances. It means that every view controller has su subclass BaseViewController. BaseViewController override prepareForSegue, and based on segue destination, tableVC, collectionVC or pageVC is instantiate.

```javascript
//MARK:- Segue
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    if segue.destination.isKind(of: GenericTableViewController.self){
        self.tableVC = segue.destination as? GenericTableViewController
        }
    else if segue.destination.isKind(of: GenericCollectionViewController.self){
        self.collectionVC = segue.destination as? GenericCollectionViewController
    }
    else if segue.destination.isKind(of: GenericPageViewController.self){
        self.pageVC = segue.destination as? GenericPageViewController
    }
}
```
So that means we have list view contorller instance, and we need just to setup it in view controller.

##### Source data #####

Source data will populate list, so we need to know with **WHAT** is going to list be populate, and **HOW** will that look like. Because of lists are generated of *SectionData* and every SectionData has it *RowData(s)*
**SectionData** present as name says, data for one section in list. List can contain several sections.
Elements of sections are:
```javascript
var title:String?
var headerReuseIdentifier: String?
var footerReuseIdentifier: String?
var headerFooterData: Any?
var rowDatas = [RowData]()
var pagination: Bool?

var headerHeight: CGFloat?
var footerHeight: CGFloat?
```
Title - *default title for header*
HeaderReuseIdentifier - *Xib and class name for header reuse view*
FooterReuseIdentifier - *Xib and class name for footer reuse view*
HeaderFooterData - *Data which will populate header/footer reuse view*
RowDatas - *Array of cells data*
Pagination - *If true, end of section will trigger loading data function*
FooterHeight - *Explicit footer height*
HeaderHeight - *Explicit header height*

**RowData** present data for one cell. Object contain data for cell xib reuse identifier and data for populaitng cell

```javascript
var cellIdentifier: String?
var cellData: Any?
var cellHeight: CGFloat?
var accordionCells: AccordionData?
```
CellIdentifier - *Reuse identifier of cell*
CellData - *Object which populate cell*

##### GenericTableViewController setup #####

To implement TableVC in your view contorller, this is madatory:
* Your view controller needs to subclass BaseViewController
* ViewController in sotryboard need to be connected to GenericTableView sceen through container View and embeded segue

In implementation of view controller, you need only to set dataSource. When sublcassing BaseViewController, you will have tableVC propery and tableVC.dataSource. So implementation of looks like this:

```javascript
person = Person()
let cell = RowData(cellId: "FirstTableViewCell", cellData: person)
let cell2 = RowData(cellId: "FirstTableViewCell", cellData: "Bye")
let cell3 = RowData(cellId: "FirstTableViewCell", cellData: "Qwerw")
let section = SectionData("First section", rowDatas: [cell, cell2, cell3])

let cell21 = RowData(cellId: "SecondTableViewCell", cellData: nil)
let cell22 = RowData(cellId: "SecondTableViewCell", cellData: nil)
let cell23 = RowData(cellId: "SecondTableViewCell", cellData: nil)
let section2 = SectionData("Second section", rowDatas: [cell12, cell22, cell23])

self.tableVC?.tableView.allowsMultipleSelection = true
self.tableVC?.tableViewConfig([section, section2],
    itemSelected: { (selectedItem) in
    print("Selected item: \(selectedItem ?? "")")
},
    itemDeselected: { (deselectItem) in
    print("Deselect item: \(deselectItem ?? "")")
},
    itemDeleted: { (deletedItem) in
    print("Deleted item : \(deletedItem ?? "")")
})
```
This tableVC will have 2 sections wit 3 cells. If you want to use custom header/footer, set SectionData footer/header reuse identifier.
Config method has 3 closure. It is callback for selection, deselection and when item is deleted.
Also, tableVC has property *.selectedSectionsData*, which hold all selected data by sections.

**Cell Height** - If cell size is not set, cell will use automatic size

**Pagination** - GenericTableViewController has *.pullToRefresh* property which can be set, and on pull method *loadingData()* is called. Subclassing BaseViewController, *loadingData* is method for fetching data for populating table view, so it should be overwritten.

// GenericTableView protocol
**EditingTableCellProtocol** - Impementing this protocol, you can use cell editing on swipe. In view controller where you implement protocol, you can set actions and callback for those actions.

```javascript
extension UIViewController : EditingTableCellProtocol{

    func editinCellActions(tableVC: GenericTableViewController, indexPath: IndexPath) -> [UITableViewRowAction] {

        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
            print("more button tapped")
        }
        more.backgroundColor = .lightGray

        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            print("favorite button tapped")
        }
        favorite.backgroundColor = .orange

        let share = UITableViewRowAction(style: .destructive, title: "Share") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = .blue

        return [share, favorite, more]
    }
}
```

**Delete cell** - Every cell can be deleted from itself (*deleteCell()*), or tableVC can remove cell by calling *deleteCell(indexPath: IndexPath)*


##### GenericCollectionViewController setup #####

To implement CollectionVC in your view contorller, this is madatory:
* Your view controller needs to subclass BaseViewController
* ViewController in storyboard need to be connected to GenericCollectionView sceen through container View and embeded segue

In implementation of view controller, you need only to set dataSource. When sublcassing BaseViewController, you will have collectionVC property and collectionVC.dataSource. So implementation of looks like this:
```javascript
let cell = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
let cell2 = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
let cell3 = RowData(cellId: "FirstCollectionViewCell", cellData: nil)
let section = SectionData("First section", rowDatas: [cell, cell2, cell3])

let cell12 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
let cell22 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
let cell23 = RowData(cellId: "SecondCollectionViewCell", cellData: nil)
let section2 = SectionData("Second section", rowDatas: [cell12, cell22, cell23])

self.collectionVC?.collectionViewConfig([section, section2],
    itemSelected: { (data) in
        print(data as Any)
},
    itemDeselected: { (data) in
        print(data as Any)
},
    itemDeleted: { (data) in
        print(data as Any)
})
```
This tableVC will have 2 sections wit 3 cells. If you want to use custom header/footer, set SectionData footer/header reuse identifier.
Config method has 3 closure. It is callback for selection, deselection and when item is deleted.
Also, tableVC has property *.selectedSectionsData*, which hold all selected data by sections.

It is same as TableVC! :)

**Cell height** - If size is not defined, cell will be automatci dimension.

**Pagination** - GenericCollectionViewController has *.pullToRefresh* property which can be set, and on pull method *loadingData()* is called. Subclassing BaseViewController, *loadingData* is method for fetching data for populating table view, so it should be overwritten.

**Delete cell** - Every cell can be deleted from itself (*deleteCell()*), or tableVC can remove cell by calling *removeItem(indexPath: IndexPath)*


##### GenericPageViewController setup #####

Generic page View controller is created to overcome page controller problem and to integrate header and content in one view, so we can handle it from one place. We need only to config data, so we can use it.

```javascript
let header = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "First tab")
let header2 = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "Second tab")
let header3 = RowData(cellId: "PageHeaderCollectionViewCell", cellData: "Second tab")

let cell = RowData(cellId: "FirstTableViewCell", cellData: nil)
let cell1 = RowData(cellId: "SecondTableViewCell", cellData: nil)
let cell2 = RowData(cellId: "FirstTableViewCell", cellData: nil)
let cell3 = RowData(cellId: "SecondTableViewCell", cellData: nil)
let cell4 = RowData(cellId: "FirstTableViewCell", cellData: nil)

let section = SectionData("First section", rowDatas: [cell, cell1, cell2, cell3, cell4])
let section1 = SectionData("Second section", rowDatas: [cell, cell2, cell5])
let section2 = SectionData("Third section", rowDatas: [cell, cell1, cell1, cell5])

let page1 = PageContentSection(pageSections: [section, section1])
let page2 = PageContentSection(pageSections: [section])
let page3 = PageContentSection(pageSections: [section1])

let pageControllerData = PageData(headerData: [header,header2, header3], contentData: [page1, page2, page3])

self.pageVC?.configPageController(pageData: pageControllerData, currentPage: { (currentPage) in
    print("View controller current page: \(currentPage)")
}, offset: { (offset) in
    print("View controller current offset: \(offset)")
})
```

##### Accordion #####

 To add accordion, set for RowData object **accordionCells**. AccordionCells is array of RowData. Only cells with data (RowData)with accordionCells have option to expend/ collaps.
If you set multiple selection of table view, you can expand multiple cells, otherwise only one cell can expand.
 

### TODO: ###

 - Accordion
 - Right swipe actions
 
 
### Who do I talk to? ###

* Dusan Cucurevic, iOS developer
cucurevicd@gmail.com
dusan.cucurevic@quantox.com

